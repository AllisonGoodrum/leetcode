using System.Threading;

public class Foo {
    
        private static AutoResetEvent event_1 = new AutoResetEvent(false);
        private static AutoResetEvent event_2 = new AutoResetEvent(false);
    public Foo() {
        
    }

    public void First(Action printFirst) {
        
        // printFirst() outputs "first". Do not change or remove this line.
        printFirst();
        event_1.Set();
    }

    public void Second(Action printSecond) {
        
        event_1.WaitOne();
        // printSecond() outputs "second". Do not change or remove this line.
        printSecond();
        event_2.Set();
    }

    public void Third(Action printThird) {
        
        event_2.WaitOne();
        // printThird() outputs "third". Do not change or remove this line.
        printThird();
    }
}