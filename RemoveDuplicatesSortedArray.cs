public class Solution {
    public int RemoveDuplicates(int[] nums) {

         // where we should put the first number because the first is always unique
        int index = 1;

        // go through all elements and compare ith with ith + 1
        for (int i = 0; i < nums.Length -1; i++) {

         // if adjacent numbers are not the same
            if (nums[i] != nums[i + 1]) {

        // place current number at the next index
        // index should hold the last unique variable in our array
                nums[index++] = nums[i + 1];

            }
        }
        return index;
    }
}

