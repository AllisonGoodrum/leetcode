public class Solution {
    public bool IsValid(string s) {
        Stack<char> mystack = new Stack<char>(); 
        
        for(int i =0 ; i < s.Length; ++i) { 
           if(s[i] == '(' || s[i] == '[' || s[i] == '{') { 
                mystack.Push(s[i]);
           }else{ 
                if(mystack.Count == 0) { 
                    return false;
                }
                if(s[i] == ')' && mystack.Pop() == '(')continue;
                if(s[i] == '}' && mystack.Pop() == '{')continue;
                if(s[i] == ']' && mystack.Pop() == '[')continue;
                return false;
           }
           
        }
        if(mystack.Count != 0) { 
                return false;
        }
        return true;
    }
}