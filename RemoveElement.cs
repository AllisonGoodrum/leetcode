public class Solution {
    public int RemoveElement(int[] nums, int val) {
        // where we start our index
        int index = 0;
        //go through all elements
        for (int i = 0; i < nums.Length; i++) {
        // if element does not equal val
            if (nums[i] != val) 
            {
        // index equals that element
                nums[index] = nums[i];
        // increment the index variable
                    index++;
            }
        }
        return index;
    }
}